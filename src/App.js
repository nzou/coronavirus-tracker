import React from 'react';

import { Cards, Graphs, Country } from './components'
import styles from './App.module.css';
import { getData } from './api';

import covidImage from './images/image.png';


class App extends React.Component {
  state = {
    data: {},
    country: '',
  }

  async componentDidMount() {
    const fetchedData = await getData();
    this.setState({ data: fetchedData });
  }

  changeCountryHandler = async (country) => {
    const fetchedData = await getData(country);
    this.setState({ data: fetchedData, country: country });

  }

  render() {
    // destructure from state
    const { data, country } = this.state;
    return (
      <div className={styles.container}>
        <img className={styles.image} src={covidImage} alt="COVID"/>
        <Cards data={data} />
        <Country changeCountryHandler={this.changeCountryHandler}/>
        <Graphs data={data} country={country}/>
      </div>
    )
  }
}

export default App;
