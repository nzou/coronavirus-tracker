// axios is used to make api requests
import axios from 'axios';
const url = 'https://covid19.mathdro.id/api'

// async function returning response from api
export const getData = async (country) => {
    let updateURL = url;

    if (country) {
        updateURL = `${url}/countries/${country}`
    }

    try {
        const { data: { confirmed, recovered, deaths, lastUpdate } } = await axios.get(updateURL);
        // only get relevant data from api
        const specificData = {
            confirmed,
            recovered,
            deaths,
            lastUpdate,
        }
        return specificData;
    } catch (error) {
        console.log(error);
    }
}

export const getDailyData = async () => {
    try {
        const { data } = await axios.get(`${url}/daily`);
        const modify = data.map((dailyData) => ({
            confirmed: dailyData.confirmed.total,
            deaths: dailyData.deaths.total,
            date: dailyData.reportDate,
        }));

        return modify;

    } catch (error) {

    }
}

export const getCountries = async () => {
    try {
        const { data: { countries } } = await axios.get(`${url}/countries`);
        // get names of countries
        return countries.map((country) => country.name);
    } catch(error) {
        console.log(error);

    }
}
