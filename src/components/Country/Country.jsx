import React, { useState, useEffect } from 'react';
import { NativeSelect, FormControl } from '@material-ui/core';
import styles from './Country.module.css';

import { getCountries } from '../../api';

const Country = ({ changeCountryHandler }) => {

    const [fetchedCountries, setFetchedCountries] = useState([]);
    useEffect(() => {
        // get all 198 countries
        const getAPI = async () => {
            setFetchedCountries(await getCountries());
        }

        getAPI();
    }, [setFetchedCountries]);

    return (
        <FormControl className={styles.FormControl}>
            <NativeSelect defaultValue="" onChange={(event) => changeCountryHandler(event.target.value)}>
                <option value="">Global</option>
                {/*create option for all countries*/}
                {fetchedCountries.map((country, index) => <option key={index} value={country}>{country}</option>)}
            </NativeSelect>
        </FormControl>
    )
}

export default Country;