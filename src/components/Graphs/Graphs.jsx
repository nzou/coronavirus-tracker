import React, { useState, useEffect } from 'react';
import { getDailyData } from '../../api';
import { Line, Bar } from 'react-chartjs-2';
import styles from './Graphs.module.css';

const Charts = ({ data: { confirmed, recovered, deaths }, country }) => {
    const [dailyData, setDailyData] = useState({});

    useEffect(() => {
        const getAPI = async () => {
            setDailyData(await getDailyData());
        }

        getAPI();
    }, []);

    const lineGraph = (
        //if array is not empty then get data for chart
        dailyData.length ?
            (<Line
                data={{
                    // return array of dates
                    labels: dailyData.map(({ date }) => date),
                    datasets: [{
                        data: dailyData.map(({ confirmed }) => confirmed),
                        label: 'Infected',
                        borderColor: '#3333ff',
                        fill: true,
                    }, {
                        data: dailyData.map(({ deaths }) => deaths),
                        label: 'Deaths',
                        borderColor: 'red',
                        backgroundColor: 'rgba(255, 0, 0, 0.5)',
                        fill: true,
                    }],
                }}

            />) : null
    );

    // produce barGraph for selected country
    const barGraph = (
        confirmed ? 
        (
            <Bar
                data={{
                    labels: ['Infected', 'Recovered', 'Deaths'],
                    datasets: [{
                        label: 'People',
                        backgroundColor: [
                            'rgba(0, 0, 255, 0.5)',
                            'rgba(0, 255, 0, 0.5)',
                            'rgba(255, 0, 0, 0.5)',
                        ],
                        data: [confirmed.value, recovered.value, deaths.value]
                    }]
                }}
                options={{
                    legend: { display: false },
                    title: { display: true, text: `Current stats in ${country}` },
                }}
            
            />
        ) : null

    );

    

    return (
        <div className={styles.container}>
            {country ? barGraph : lineGraph}
        </div>
    )
}

export default Charts;